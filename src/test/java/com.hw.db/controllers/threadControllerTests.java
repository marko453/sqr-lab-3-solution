package com.hw.db.controllers;
import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.models.Post;
import com.hw.db.models.Thread;
import com.hw.db.models.User;
import com.hw.db.models.Vote;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.stubbing.Answer;
import java.time.Instant;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;

class threadControllerTests {
    private threadController controller;
    private final Timestamp timestamp = Timestamp.from(Instant.now()); // Timestamp
    private final String testAuth = "test author"; // Author
    private final String testSlg = "test-slug"; // Slug
    private final String testForum = "test forum"; // Forum
    private final String testMsg = "My Test Message"; // Message
    private final User testUsr = new User(testAuth, "test@example.com", "Test Fullname", ""); // User Info
    private Thread testThrd; // Thread
    private final Integer testThrdId = 0; // Thread ID
    private final Post testPost = new Post(testAuth, timestamp, testForum, testMsg, 0, 0, false); // Post
    private final List<Post> testPosts = List.of(testPost); // List of posts
    private Vote testVote = new Vote(testAuth, 1);

    @BeforeEach
    @DisplayName("Thread Controller (Test)")
    void threadTest() {
        controller = new threadController();
        testThrd = new Thread(testAuth, timestamp, testForum, testMsg, testSlg, "Thread Title", 0);
        testThrd.setId(testThrdId);
    }

    @Test
    @DisplayName("Post Creation (Test)")
    void correctlyCreatePost() {
        try (MockedStatic<ThreadDAO> threadDaoMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadDaoMock
                    .when(() -> ThreadDAO.createPosts(Mockito.any(), Mockito.any(), Mockito.any()))
                    .thenAnswer((Answer<Void>) invocation -> null);
            threadDaoMock
                    .when(() -> ThreadDAO.getThreadById(Mockito.any()))
                    .thenReturn(testThrd);
            threadDaoMock
                    .when(() -> ThreadDAO.getThreadBySlug(Mockito.any()))
                    .thenReturn(testThrd);

            try (MockedStatic<UserDAO> usedDaoMock = Mockito.mockStatic(UserDAO.class)) {
                usedDaoMock
                        .when(() -> UserDAO.Info(Mockito.any()))
                        .thenReturn(testUsr);
                controller.createPost(testSlg, testPosts);
                threadDaoMock.verify(() -> ThreadDAO.createPosts(testThrd, testPosts, List.of(testUsr)));
            }
        }
    }

    @Test
    @DisplayName("Get Post (Test)")
    void correctlyReturnPost() {
        try (MockedStatic<ThreadDAO> threadDaoMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadDaoMock
                    .when(() -> ThreadDAO.getPosts(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any()))
                    .thenAnswer((Answer<List<Post>>) invocation -> List.of(testPost));
            threadDaoMock
                    .when(() -> ThreadDAO.getThreadById(Mockito.any()))
                    .thenReturn(testThrd);
            threadDaoMock
                    .when(() -> ThreadDAO.getThreadBySlug(Mockito.any()))
                    .thenReturn(testThrd);
            Integer since = null;
            String sort = null;
            controller.Posts(testSlg, 100, since, sort, false);
            threadDaoMock.verify(() -> ThreadDAO.getPosts(testThrdId, 100, since, sort, false));
        }
    }

    @Test
    @DisplayName("Change Post (Test)")
    void correctlyChangePost() {
        try (MockedStatic<ThreadDAO> threadDaoMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadDaoMock
                    .when(() -> ThreadDAO.change(Mockito.any(Thread.class), Mockito.any(Thread.class)))
                    .thenAnswer((Answer<Void>) invocation -> null);
            threadDaoMock
                    .when(() -> ThreadDAO.getThreadById(Mockito.any()))
                    .thenReturn(testThrd);
            threadDaoMock
                    .when(() -> ThreadDAO.getThreadBySlug(Mockito.any()))
                    .thenReturn(testThrd);
            try (MockedStatic<UserDAO> usedDaoMock = Mockito.mockStatic(UserDAO.class)) {
                usedDaoMock
                        .when(() -> UserDAO.Info(Mockito.any()))
                        .thenReturn(testUsr);
                controller.change(testSlg, testThrd);
                threadDaoMock.verify(() -> ThreadDAO.change(testThrd, testThrd));
            }
        }
    }

    @Test
    @DisplayName("Get Thread Info (Test)")
    void correctlyGetThreadInfo() {
        try (MockedStatic<ThreadDAO> threadDaoMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadDaoMock
                    .when(() -> ThreadDAO.change(Mockito.any(Thread.class), Mockito.any(Thread.class)))
                    .thenAnswer((Answer<Void>) invocation -> null);
            threadDaoMock
                    .when(() -> ThreadDAO.getThreadById(Mockito.any()))
                    .thenReturn(testThrd);
            threadDaoMock
                    .when(() -> ThreadDAO.getThreadBySlug(Mockito.any()))
                    .thenReturn(testThrd);
            controller.info(testSlg);
            threadDaoMock.verify(() -> ThreadDAO.getThreadBySlug(testSlg));
        }
    }

    @Test
    @DisplayName("Create Vote (Test)")
    void correctlyCreateVote() {
        try (MockedStatic<ThreadDAO> threadDaoMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadDaoMock
                    .when(() -> ThreadDAO.change(Mockito.any(Thread.class), Mockito.any(Thread.class)))
                    .thenAnswer((Answer<Void>) invocation -> null);
            threadDaoMock
                    .when(() -> ThreadDAO.getThreadById(Mockito.any()))
                    .thenReturn(testThrd);
            threadDaoMock
                    .when(() -> ThreadDAO.getThreadBySlug(Mockito.any()))
                    .thenReturn(testThrd);
            try (MockedStatic<UserDAO> usedDaoMock = Mockito.mockStatic(UserDAO.class)) {
                usedDaoMock
                        .when(() -> UserDAO.Info(Mockito.any()))
                        .thenReturn(testUsr);
                controller.createVote(testSlg, testVote);
                threadDaoMock.verify(() -> ThreadDAO.createVote(testThrd, testVote));
            }
        }
    }
}