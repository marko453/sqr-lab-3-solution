package com.hw.db.controllers;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.models.Thread;
import com.hw.db.models.*;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@RestController
@CrossOrigin(origins = {"http://localhost:3000"}, allowCredentials = "true")
@RequestMapping("/api/thread")

public class threadController {

    private Thread CheckIdOrSlug (String slug_or_id) {
        if (slug_or_id.matches("\\d+")) {
            return ThreadDAO.getThreadById(Integer.parseInt(slug_or_id));
        } else {
            return ThreadDAO.getThreadBySlug(slug_or_id);
        }
    }

    @PostMapping(path = "/{slug_or_id}/create" ,consumes = "application/json", produces = "application/json")
    public ResponseEntity createPost(@PathVariable(name="slug_or_id") String slug, @RequestBody List<Post> posts){
        Thread thr = new Thread();
        List<User> users = new ArrayList<>();
        try {
            thr = CheckIdOrSlug(slug);
            int i = 0;
            for (Post post:posts) {
                post.setForum(thr.getForum());
                users.add(UserDAO.Info(post.getAuthor()));
                post.setAuthor(users.get(i).getNickname());
                post.setThread(thr.getId());
                i++;
            }
        }
        catch (DataAccessException Exc) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new Message("Section not found."));
        } try {
            ThreadDAO.createPosts(thr, posts, users);
        } catch (DataAccessException Exc) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(new Message("At least one parent post is missing from the current discussion thread."));
        }
        return ResponseEntity.status(HttpStatus.CREATED).body(posts);
    }

    @GetMapping(path="/{slug_or_id}/posts")
    public ResponseEntity Posts(@PathVariable(name="slug_or_id") String slug, @JsonProperty("limit") Integer limit, @JsonProperty("since") Integer since,@JsonProperty("sort") String sort,@JsonProperty("desc") Boolean desc){
        Thread thr;
        try {
            thr = CheckIdOrSlug(slug);
        } catch (DataAccessException Exc) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new Message("Section not found."));
        }
        List<Post> res = ThreadDAO.getPosts(thr.getId(), limit, since, sort, desc);
        return ResponseEntity.status(HttpStatus.OK).body(res);
    }

    @PostMapping(path="/{slug_or_id}/details")
    public ResponseEntity change(@PathVariable(name="slug_or_id") String slug,@RequestBody Thread thread){
        Thread thr = new Thread();
        try {
            thr = CheckIdOrSlug(slug);
            ThreadDAO.change(thr, thread);
        } catch (DataAccessException Exc) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new Message("Section not found."));
        }
        thr = CheckIdOrSlug(thr.getId().toString());
        return ResponseEntity.status(HttpStatus.OK).body(thr);
    }

    @GetMapping(path="/{slug_or_id}/details")
    public ResponseEntity info(@PathVariable(name="slug_or_id") String slug){
        Thread thr = new Thread();
        try {
            thr = CheckIdOrSlug(slug);
        } catch (DataAccessException Exc) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new Message("Section not found."));
        }
        return ResponseEntity.status(HttpStatus.OK).body(thr);
    }

    @PostMapping(path = "/{slug_or_id}/vote", consumes = "application/json", produces = "application/json")
    public ResponseEntity createVote(@PathVariable(name="slug_or_id") String slug, @RequestBody Vote vote){
        Thread thr = new Thread();
        Integer votes = 0;
        try {
            thr = CheckIdOrSlug(slug);
            votes = thr.getVotes();
            User user = UserDAO.Info(vote.getNickname());
            vote.setNickname(user.getNickname());
            vote.setTid(thr.getId());
        }
        catch (DataAccessException Exc) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new Message("Section not found."));
        } try {
            ThreadDAO.createVote(thr, vote);
            votes += vote.getVoice();
        } catch (DuplicateKeyException Exc) {
            try {
                votes = ThreadDAO.change(vote, votes);
            } catch (DuplicateKeyException Except) {
                return ResponseEntity.status(HttpStatus.CONFLICT).body(new Message("At least one parent post is missing from the current discussion thread."));
            }
        }
        thr.setVotes(votes);
        return ResponseEntity.status(HttpStatus.OK).body(thr);
    }
}
